#include <iostream>
#include <fstream>
#include <string.h>
#include "Color.h"
#include "../ParamSet/ParamSet.h"

using std::string;
using std::ofstream;

class Film
{
	int x_res;
	int y_res;
	string type;
	string filename;
	string imagetype;
	string img_type;

	ofstream image_file;

	Color * buffer;

	public:
	Film(ParamSet & ps)
	{
		x_res = ps.search<int>("x_res", 200);
		y_res = ps.search<int>("y_res", 200);
		type = ps.search<string>("type", "image");
		filename = ps.search<string>("filename", "image_out.ppm");
		img_type = ps.search<string>("img_type", "ppm");

		buffer = new Color[x_res*y_res];
	}

	virtual ~Film() { delete[] buffer; }

	int width() { return x_res; }
	int height() { return y_res; }

	void add(int x, int y, Color c) { *(buffer + y*x_res + x) = c; }

	void write_image()
	{
		image_file.open(filename);

		image_file << "P3\n" << x_res << " " << y_res << "\n255\n";

		int i = 0;
		while(i < x_res*y_res)
		{
			image_file << (buffer + i)->channels[R] << " " << (buffer + i)->channels[G] << " " << (buffer + i)->channels[B] << "\n";
			i++;
		}

		image_file.close();
	}
};

class Camera
{
	string type;
	float fovx;
	float fovy;
};