#include <iostream>
#include <string.h>
#include <sstream>
#include "tinyxml2.h"
#include "tinyxml2.cpp"

using std::string;
using std::stoi;
using namespace tinyxml2;

class Parser
{
	private:
	XMLDocument doc;
	XMLElement * base;

	void insert_color(string side, XMLElement * xml_p, ParamSet & ps)
	{
		char * cside = &side[0];
		if(xml_p->Attribute(cside) != nullptr)
		{
			std::stringstream c_values;
			c_values << xml_p->Attribute(cside);

			Color color;
			c_values >> color.channels[R] >> color.channels[G] >> color.channels[B];
			ps.add<Color>(side, &color);
		}
	}

	public:
	Parser()
	{
		doc.LoadFile("scene.xml");
		base = doc.FirstChildElement();
	}

	ParamSet load_film()
	{
		XMLElement * film = base->FirstChildElement("film");

		ParamSet ps;
		
		if(film->Attribute("type") != nullptr) 
		{
			string type = film->Attribute("type");
			ps.add<string>("type", &type);
		}
		if(film->Attribute("x_res") != nullptr)
		{
			int x_res = film->IntAttribute("x_res");
			ps.add<int>("x_res", &x_res);
		}
		if(film->Attribute("y_res") != nullptr)
		{
			int y_res =  film->IntAttribute("y_res");
			ps.add<int>("y_res", &y_res);
		}
		if(film->Attribute("filename") != nullptr)
		{
			string filename = film->Attribute("filename");
			ps.add<string>("filename", &filename);
		}
		if(film->Attribute("img_type") != nullptr)
		{
			string img_type = film->Attribute("img_type");
			ps.add<string>("img_type", &img_type);
		}

		return ps;
	}

	ParamSet load_background()
	{
		XMLElement * background = base->FirstChildElement("background");

		ParamSet ps;

		if(background->Attribute("type") != nullptr)
		{
			string type = background->Attribute("type");
			ps.add<string>("type", &type);
		}

		if(background->Attribute("mapping") != nullptr)
		{
			string mapping = background->Attribute("mapping");
			ps.add<string>("mapping", &mapping);
		}
		
		insert_color("bl", background, ps);
		insert_color("br", background, ps);
		insert_color("tl", background, ps);
		insert_color("tr", background, ps);
		insert_color("color", background, ps);

		return ps;
	}
};