#include <iostream>
#include "Film.h"
#include "Background.h"
#include "Parser.h"

using std::cout;
using std::string;

int main()
{
	Parser parser;
	ParamSet psf = parser.load_film();
	ParamSet psbg = parser.load_background();

	Film film(psf);
	Background bg(psbg);

	int h = film.height();
	int w = film.width();

	for(int j = h-1; j > 0; j--)
	{
		for(int i = 0; i < w; i++)
		{
			channel_type cj = (channel_type)j;
			channel_type ci = (channel_type)i;
			Color c = { ci, cj, 174 };
			film.add(i, j, c);
		}
	}



	film.write_image();

	return 0;
}