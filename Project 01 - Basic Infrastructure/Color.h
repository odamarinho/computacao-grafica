using channel_type = unsigned short int;

enum { R=0, G=1, B=2 };
class Color 
{
	public:
	channel_type channels[3];
};

std::ostream & operator<<(std::ostream & os, const Color & c)
{
	os << "{ " << c.channels[R] << ", " << c.channels[G] << ", " << c.channels[B] << " }";
	return os;
}
