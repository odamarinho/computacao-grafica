#include <iostream>
#include <string.h>

using std::string;

class Background
{
	string type;
	string mapping;
	Color bl;
	Color br;
	Color tl;
	Color tr;
	Color color;

	public:
	Background(ParamSet & ps)
	{
		type = ps.search<string>("type", "colors");
		mapping = ps.search<string>("mapping", "screen");
		bl = ps.search<Color>("bl", { 0, 0, 0 });
		br = ps.search<Color>("br", { 0, 0, 0 });
		tl = ps.search<Color>("tl", { 0, 0, 0 });
		tr = ps.search<Color>("tr", { 0, 0, 0 });
		color = ps.search<Color>("color", { 0, 0, 0 });
	}

	Color sample(float x, float y)
	{
		
	}
};
