#include <iostream>
#include "ParamSet.h"

using std::cout;
using std::string;

int main()
{
	ParamSet ps;
	cout << "tamanho: " << ps.lenght() << "\n";

	int * ints = new int[3];
	string * ss = new string[5];
	ss[0] = "Ortografiquinha";
	ss[1] = "60 54 348";
	ss[2] = "200";
	ss[3] = "400";
	ss[4] = "teste";
	ints[0] = 0;
	ints[1] = 1;
	ints[2] = 2;

	ps.add<int>("chave1", ints, 3);
	ps.add<string>("chave2", ss, 5);

	cout << "tamanho: " << ps.lenght() << "\n\n";

	void * r = ps.search("chave1");
	void * r2 = ps.search("chave2");

	int i = 0;
	byte * b = (byte*)r;
	while(i < 3*sizeof(int))
	{
		int * n = (int*)(b+i);
		cout << *n << "\n";

		i += sizeof(int);
	}

	i = 0;
	b = (byte*)r2;
	while(i < 5*sizeof(string))
	{
		string * s = (string*)(b+i);
		cout << *s << "\n";

		i += sizeof(string);
	}

	delete[] ints;
	delete[] ss;
	return 0;
}