#include <iostream>
#include <string.h>
#include <unordered_map>
#include <cstring>
#include <iterator>

using byte = unsigned char;
using std::string;
using std::unordered_map;


class ParamSet
{
	int mi_length = 0;
	unordered_map<string, byte*> mpt_data;

	public:
	template<typename T>
	void add(const string & key, T * item)
	{
		T * data = new T;
		*data = *item;
		byte * data_bytes = (byte*)data;

		mpt_data[key] = data_bytes;
		mi_length++;
	}

	template<typename T>
	T search(const string & key, const T & default_)
	{
		if(mpt_data.find(key) != mpt_data.end())
		{
			return *((T*)(mpt_data[key]));
		}

		return default_;
	}

	bool erase(const string & key)
	{
		if (mpt_data.find(key) != mpt_data.end())
		{
			delete mpt_data[key];
			mpt_data.erase(key);

			return true;
		}

		return false;
	}

	void clear()
	{
		unordered_map<string, byte*>::iterator it = mpt_data.begin();
		while(it != mpt_data.end())
		{
			erase(it->first);
			it++;
		}
	}

	int lenght() { return mi_length; }

	virtual ~ParamSet()
	{
		unordered_map<string, byte*>::iterator it = mpt_data.begin();
		while(it != mpt_data.end()) 
		{
			delete it->second;
			it++;
		}
	}
};
